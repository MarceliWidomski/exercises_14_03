//============================================================================
// Name        : adds_substracs_vectors.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	int maxSize(1000);
	int vector1[maxSize];
	int vector2[maxSize];
	cout
			<< "Program adds and substracts 2 vectors which number of dimentions is smaller or equal to 1000"
			<< endl;
	cout << "Enter number of vector dimentions: ";
	int dimentions;
	cin >> dimentions;
	cout << "Enter first vector name: ";
	char vectorName1;
	cin >> vectorName1;
	for (int i = 0; i < dimentions; ++i) {
		cout << "Enter " << i + 1 << " coordinate of vector " << vectorName1
				<< ": ";
		cin >> vector1[i];
	}
	cout << "Enter second vector name: ";
	char vectorName2;
	cin >> vectorName2;
	for (int i = 0; i < dimentions; ++i) {
		cout << "Enter " << i + 1 << " coordinate of vector " << vectorName2
				<< ": ";
		cin >> vector2[i];
	}
	int added[maxSize] = { }; //array for added coordinates
	int substracted[maxSize] = { }; // array for substracted coordinates
	for (int i = 0; i < dimentions; ++i) {
		added[i] = vector1[i] + vector2[i];
		substracted[i] = vector1[i] - vector2[i];
	}
	cout << "Vector " << vectorName1 << " plus vector " << vectorName2
			<< " equals vector" << endl;
	for (int i = 0; i < dimentions; ++i) {
		cout << added[i] << " ";
	}
	cout << endl;
	cout << "Vector " << vectorName1 << " minus vector " << vectorName2
			<< " equals vector" << endl;
	for (int i = 0; i < dimentions; ++i) {
		cout << substracted[i] << " ";
	}
	return 0;
}
