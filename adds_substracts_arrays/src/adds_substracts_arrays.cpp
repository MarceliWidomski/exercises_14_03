//============================================================================
// Name        : adds_substracts_arrays.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	const int maxSize(100);
	int firstArray[maxSize][maxSize] = { 0 };
	int secondArray[maxSize][maxSize] = { 0 };
	cout << "Program adds and substracts 2 arrays" << endl;
	cout << "Enter number of rows: ";
	int rowsNr;
	cin >> rowsNr;
	cout << "Enter number of columns: ";
	int columnsNr;
	cin >> columnsNr;
	cout << "Enter name of first array: ";
	char firstName;
	cin >> firstName;
	cout << "Enter elements to " << firstName << endl;
	for (int i = 0; i < rowsNr; ++i) {
		for (int j = 0; j < columnsNr; ++j) {
			cout << "Enter element with index " << firstName << " " << i + 1
					<< " " << j + 1 << ": ";
			cin >> firstArray[i][j];
		}
		cout << endl;
	}
	cout << "Enter name of second array: ";
	char secondName;
	cin >> secondName;
	cout << "Enter elements to " << secondName << endl;
	for (int i = 0; i < rowsNr; ++i) {
		for (int j = 0; j < columnsNr; ++j) {
			cout << "Enter element with index " << secondName << " " << i + 1
					<< " " << j + 1 << ": ";
			cin >> secondArray[i][j];
		}
		cout << endl;
	}
	int addedArray[maxSize][maxSize] = { 0 };
	int substractedArray[maxSize][maxSize] = { 0 };
	for (int i = 0; i < rowsNr; ++i) {
		for (int j = 0; j < columnsNr; ++j) {
			addedArray[i][j] = firstArray[i][j] + secondArray[i][j];
			substractedArray[i][j] = firstArray[i][j] - secondArray[i][j];
		}
	}
	cout << "Array " << firstName << " plus array " << secondName << " equals: "
			<< endl;
	for (int i = 0; i < rowsNr; ++i) {
		for (int j = 0; j < columnsNr; ++j) {
			cout << addedArray[i][j] << " ";
		}
		cout << endl;
	}
	cout << "Array " << firstName << " minus array " << secondName
			<< " equals: " << endl;
	for (int i = 0; i < rowsNr; ++i) {
		for (int j = 0; j < columnsNr; ++j) {
			cout << substractedArray[i][j] << " ";
		}
		cout << endl;
	}
	return 0;
}
