//============================================================================
// Name        : array_determinant.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	const int maxSize(100);
	double array[maxSize][maxSize] = { 0 };
	cout
			<< "Program calculates square array determinant by Gaussian Elimination"
			<< endl;
	cout << "Enter name of array: ";
	char name;
	cin >> name;
	cout << "Enter size of array " << name << ": ";
	int size;
	cin >> size;
	cout << "Enter elements to " << name << endl;
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			cout << "Enter element with index " << name << " " << i + 1 << " "
					<< j + 1 << ": ";
			cin >> array[i][j];
		}
		cout << endl;
	}
	double echelonForm[maxSize][maxSize];
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			echelonForm[i][j] = array[i][j];
		}
	}
	for (int i = 0; i < size; ++i) {
		for (int j = i + 1; j <= size; ++j) {
			for (int k = i + 1; k <= size; k++) {
				echelonForm[j][k] -= echelonForm[i][k]
						* (echelonForm[j][i] / echelonForm[i][i]);
			}
		}
	}
	double det(1);
	for (int i = 0; i < size; ++i)
		det *= echelonForm[i][i];
	cout << "Det " << name << " = " << det << endl;
	return 0;
}
