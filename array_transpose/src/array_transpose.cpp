//============================================================================
// Name        : array_transpose.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	const int maxSize(100);
	int array[maxSize][maxSize] = { 0 };
	cout << "Program transposes array" << endl;
	cout << "Enter number of rows: ";
	int rowsNr;
	cin >> rowsNr;
	cout << "Enter number of columns: ";
	int columnsNr;
	cin >> columnsNr;
	for (int i = 0; i < rowsNr; ++i) { // fills array
		for (int j = 0; j < columnsNr; ++j) {
			cout << "Enter element with index " << i << " " << j << ": ";
			cin >> array[i][j];
		}
		cout << endl;
	}
	cout << "Array: " << endl;
	for (int i = 0; i < rowsNr; ++i) { // prints array
		for (int j = 0; j < columnsNr; ++j) {
			cout << array[i][j] << " ";
		}
		cout << endl;
	}
	rowsNr = rowsNr - columnsNr; // swaps rows number and columns number
	columnsNr = columnsNr + rowsNr;
	rowsNr = columnsNr - rowsNr;
	int transposedArray[maxSize][maxSize] = { 0 };
	for (int i = 0; i < rowsNr; ++i) { // transposes array
		for (int j = 0; j < columnsNr; ++j) {
			transposedArray[i][j] = array[j][i];
		}
	}
	cout << "Transposed array: " << endl;
	for (int i = 0; i < rowsNr; ++i) { // prints transposed array
		for (int j = 0; j < columnsNr; ++j) {
			cout << transposedArray[i][j] << " ";
		}
		cout << endl;
	}
	return 0;
}
