//============================================================================
// Name        : multiplies_arrays.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	const int maxSize(100);
	int firstArray[maxSize][maxSize] = { 0 };
	int secondArray[maxSize][maxSize] = { 0 };
	cout << "Program multiplies 2 arrays" << endl;
	cout << "Enter name of first array: ";
	char firstName;
	cin >> firstName;
	cout << "Enter array " << firstName << " number of rows: ";
	int firstRowsNr;
	cin >> firstRowsNr;
	cout << "Enter array " << firstName << " number of columns: ";
	int firstColumnsNr;
	cin >> firstColumnsNr;
	cout << "Enter elements to " << firstName << endl;
	for (int i = 0; i < firstRowsNr; ++i) {
		for (int j = 0; j < firstColumnsNr; ++j) {
			cout << "Enter element with index " << firstName << " " << i + 1
					<< " " << j + 1 << ": ";
			cin >> firstArray[i][j];
		}
		cout << endl;
	}
	cout << "Enter name of second array: ";
	char secondName;
	cin >> secondName;
	int secondRowsNr(0);
	while (secondRowsNr != firstColumnsNr) {
		cout << "Enter array " << secondName << " number of rows: ";
		cin >> secondRowsNr;
		if (secondRowsNr != firstColumnsNr) {
			cout << "Number of rows in array " << secondName
					<< " must be equal to number of columns in array "
					<< firstName << endl;
			cout << "Try again." << endl;
		}
	}
	cout << "Enter array " << secondName << " number of columns: ";
	int secondColumnsNr;
	cin >> secondColumnsNr;
	cout << "Enter elements to " << secondName << endl;
	for (int i = 0; i < secondRowsNr; ++i) {
		for (int j = 0; j < secondColumnsNr; ++j) {
			cout << "Enter element with index " << secondName << " " << i + 1
					<< " " << j + 1 << ": ";
			cin >> secondArray[i][j];
		}
		cout << endl;
	}
	int resultArray[maxSize][maxSize] = { 0 };
	int resultRowsNr(firstRowsNr);
	int resultColumnsNr(secondColumnsNr);
	for (int i = 0; i < resultRowsNr; ++i) {
		for (int j = 0; j < resultColumnsNr; ++j) {
			for (int k = 0; k < firstColumnsNr; ++k) {
				resultArray[i][j] += firstArray[i][k] * secondArray[k][j];
			}
		}
	}
	cout << "Array " << firstName << " multiplied by array " << secondName
			<< " equals: " << endl;
	for (int i = 0; i < resultRowsNr; ++i) {
		for (int j = 0; j < resultColumnsNr; ++j) {
			cout << resultArray[i][j] << " ";
		}
		cout << endl;
	}
	return 0;
}
