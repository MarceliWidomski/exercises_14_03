//============================================================================
// Name        : multiplies_vector_by_a_scalar.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	int maxSize(1000);
	int vector[maxSize];
	cout
			<< "Program multiplies vector by a scalar ( number of vector dimentions is smaller or equal to 1000)"
			<< endl;
	cout << "Enter number of vector dimentions: ";
	int dimentions;
	cin >> dimentions;
	cout << "Enter vector name: ";
	char vectorName;
	cin >> vectorName;
	for (int i = 0; i < dimentions; ++i) {
		cout << "Enter " << i + 1 << " coordinate of vector " << vectorName
				<< ": ";
		cin >> vector[i];
	}
	cout << "Emter scalar: ";
	int factor;
	cin >> factor;
	int multiplied[maxSize] = { };
	for (int i = 0; i < dimentions; ++i) {
		multiplied[i] = vector[i] * factor;
	}
	cout << "Vector " << vectorName << " times " << factor << " equals vector"
			<< endl;
	for (int i = 0; i < dimentions; ++i) {
		cout << multiplied[i] << " ";
	}
	return 0;
}
