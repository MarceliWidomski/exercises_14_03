//============================================================================
// Name        : scalar_vector_product.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	const int size(3);
	int vector1[size];
	int vector2[size];
	cout << "Program calculates scalar product and vector product" << endl;
	cout << "Enter first vector name: ";
	char vectorName1;
	cin >> vectorName1;
	for (int i = 0; i < size; ++i) {
		cout << "Enter " << i + 1 << " coordinate of vector " << vectorName1
				<< ": ";
		cin >> vector1[i];
	}
	cout << "Enter second vector name: ";
	char vectorName2;
	cin >> vectorName2;
	for (int i = 0; i < size; ++i) {
		cout << "Enter " << i + 1 << " coordinate of vector " << vectorName2
				<< ": ";
		cin >> vector2[i];
	}
	int scalarProduct(0);
	for (int i = 0; i < size; ++i)
		scalarProduct += vector1[i] * vector2[i];
	int vectorProduct[size] = { };
	vectorProduct[0] = vector1[1] * vector2[2] - vector1[2] * vector2[1];
	vectorProduct[1] = vector1[2] * vector2[0] - vector1[0] * vector2[2];
	vectorProduct[2] = vector1[0] * vector2[1] - vector1[1] * vector2[0];
	cout << "Scalar product of vector " << vectorName1 << " and vector "
			<< vectorName2 << " equals: " << scalarProduct << endl;
	cout << "Vector product of vector " << vectorName1 << " and vector "
			<< vectorName2 << " equals vector: " << endl;
	for (int i = 0; i < size; ++i) {
		cout << vectorProduct[i] << " ";
	}
	return 0;
}
