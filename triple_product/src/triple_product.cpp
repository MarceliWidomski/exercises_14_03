//============================================================================
// Name        : triple_product.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	const int size(3);
	int vector1[size];
	int vector2[size];
	int vector3[size];
	cout << "Program calculates triple product of vectors [a o (b x c)]"
			<< endl;
	cout << "Enter first vector name: ";
	char vectorName1;
	cin >> vectorName1;
	for (int i = 0; i < size; ++i) {
		cout << "Enter " << i + 1 << " coordinate of vector " << vectorName1
				<< ": ";
		cin >> vector1[i];
	}
	cout << "Enter second vector name: ";
	char vectorName2;
	cin >> vectorName2;
	for (int i = 0; i < size; ++i) {
		cout << "Enter " << i + 1 << " coordinate of vector " << vectorName2
				<< ": ";
		cin >> vector2[i];
	}
	cout << "Enter third vector name: ";
	char vectorName3;
	cin >> vectorName3;
	for (int i = 0; i < size; ++i) {
		cout << "Enter " << i + 1 << " coordinate of vector " << vectorName1
				<< ": ";
		cin >> vector3[i];
	}
	int tripleProduct(0);
	tripleProduct = vector3[0] * vector1[1] * vector2[2]
			- vector3[0] * vector1[2] * vector2[1]
			- vector3[1] * vector1[0] * vector2[2]
			+ vector3[1] * vector1[2] * vector2[0]
			+ vector3[2] * vector1[0] * vector2[1]
			- vector3[2] * vector1[1] * vector2[0];
	cout << "Triple product of vector " << vectorName1 << ", vector "
			<< vectorName2 << " and vector  " << vectorName3 << " equals: "
			<< tripleProduct << endl;
	return 0;
}
